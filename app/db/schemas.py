from typing import List

from pydantic import BaseModel

class Vehicle(BaseModel):
    id: int
    distance: int
    owner_username: str

    class Config:
        orm_mode = True


class User(BaseModel):
    user_name: str
    password: str

    class Config:
        orm_mode = True

class Token(BaseModel):
    access_token: str
    token_type: str
