from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .database import Base


class User(Base):
    __tablename__ = "users"

    user_name = Column(String, primary_key=True, index=True)
    password = Column(String)

    vehicles = relationship("Vehicle", back_populates="owner")


class Vehicle(Base):
    __tablename__ = "vehicles"

    id = Column(Integer, primary_key=True, index=True)

    distance = Column(Integer)
    owner_username = Column(String, ForeignKey("users.user_name"))

    owner = relationship("User", back_populates="vehicles")

