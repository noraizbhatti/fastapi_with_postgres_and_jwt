from sqlalchemy.orm import Session

from db import models, schemas


def get_user(db: Session, user_name: str):
    """
    Get user from DB.
    """

    return db.query(models.User).filter(models.User.user_name == user_name).first()

def get_vehicles_of_user(db: Session, user_name: str):
    """
    Get vehicles associated with the given username.
    """

    user = db.query(models.User).filter(models.User.user_name == user_name).first()
    user.vehicles.sort(key=lambda vehicle: vehicle.distance)
    return user.vehicles
