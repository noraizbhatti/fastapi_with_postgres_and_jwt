
from sqlalchemy import create_engine

from db import models
from db.database import SQLALCHEMY_DATABASE_URL, engine

def create_users():
    """
    Load users from csv to DB table.
    """

    with open('users.csv', 'r') as f:    
        conn = create_engine(SQLALCHEMY_DATABASE_URL).raw_connection()
        cursor = conn.cursor()
        cmd = 'COPY users(user_name, password) FROM STDIN WITH (FORMAT CSV, HEADER TRUE)'
        cursor.copy_expert(cmd, f)
        conn.commit()

def create_vehicles():
    """
    Load vehicles from csv to DB table.
    """

    with open('vehicles.csv', 'r') as f:    
        conn = create_engine(SQLALCHEMY_DATABASE_URL).raw_connection()
        cursor = conn.cursor()
        cmd = 'COPY vehicles(id, distance, owner_username) FROM STDIN WITH (FORMAT CSV, HEADER TRUE)'
        cursor.copy_expert(cmd, f)
        conn.commit()


if __name__ == '__main__':
    models.Base.metadata.create_all(bind=engine)
    try:
        create_users()
        create_vehicles()
    except:
        pass
