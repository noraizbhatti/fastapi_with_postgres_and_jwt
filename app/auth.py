from datetime import datetime, timedelta
import jwt
from jwt import PyJWTError

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer

from db import handler

# TODO: Set in env variables. (boilerplate to read and use env variables is required)
SECRET_KEY = "78f21b6768d7ac835e031a74652e7826f809558ce8c2e79fdb329ea1f851dd09"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 10

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")

def authenticate_user(username: str, password: str, db):
    """"
    Dumbest way possible to check for authentication.
    """

    user = handler.get_user(db, username)
    if not user:
        return False
    if not password == user.password:
        return False
    return user

def create_access_token(*, data: dict, expires_delta: timedelta = None):
    """
    Create access token. Set it to expire after a certain amount of time.
    """
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    
    return encoded_jwt

async def get_current_user(token: str = Depends(oauth2_scheme)):
    """
    Check validity of sent token. Decode and get username.
    """

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
    except PyJWTError:
        raise credentials_exception

    return username
