from typing import List

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from db import handler, schemas
from db.database import get_db

from auth import authenticate_user, create_access_token, get_current_user

app = FastAPI()


@app.post("/token", response_model=schemas.Token)
async def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)
):
    """
    Endpoint for Bearer token. 
    """
    user = authenticate_user(form_data.username, form_data.password, db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = create_access_token(data={"sub": user.user_name})
    return {"access_token": access_token, "token_type": "bearer"}


@app.get("/vehicles/", response_model=List[schemas.Vehicle])
def get_vehicles_of_user(current_user: str = Depends(get_current_user), db: Session = Depends(get_db)):
    """
    Endpoint for vehicles associated with requesting user.
    """

    users = handler.get_vehicles_of_user(db, current_user)
    return users
