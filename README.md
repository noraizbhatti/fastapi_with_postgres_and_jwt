# Fuelsave FastAPI.

## To run

Run it with docker-compose as:
>  docker-compose up

`docker-compose up` would do following:

*  Setup postgres DB.
*  Just for the sake of testing, a service "add_data_poc" would add data to postgres tables. (users and vehicles)
*  Setup FastAPI to listen at localhost port 80.

## FastAPI endpoint

Following are the 2 endpoints.

1.  POST /token which returns a Bearer token. (OAuth2)
2.  GET /vehicles to return the vehicles (sorted by distance) associated with the user.
  

## Example request and responses

### 1 - Token:
#### Request
>  curl --location --request POST 'http://0.0.0.0/token/' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'username=user1' \
--data-urlencode 'password=pass1'

#### Resposne

>  {
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyMSIsImV4cCI6MTU4OTAzODgwN30.OPjfPaYaA9qiXEUcwlpPsltCX5s4yvd20CdIldhLESQ",
    "token_type": "bearer"
}

### 2 - Vehicles

#### Request
>  curl --location --request GET 'http://0.0.0.0/vehicles/' \
--header 'Accept: application/json' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIU...'

#### Resposne


>  [
    {
        "id": 41,
        "distance": 3355,
        "owner_username": "user1"
    },
    {
        "id": 43,
        "distance": 3677,
        "owner_username": "user1"
    },
    {
        "id": 2,
        "distance": 4199,
        "owner_username": "user1"
    }, ...


## Interactive Docs

Alternatively, endpoints can be tested on /docs endpoint. 

